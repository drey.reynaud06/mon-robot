*** Settings ***

Resource    exo8.resource
Documentation       Une premier test d’accès à la page de login.
Library    SeleniumLibrary
 
Library     quash_tf.TFParamservice
*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox

*** Test Cases ***
Test 1
    [Tags]    run once
    Open browser     ${url}    ${browser}

    Given L'utilisateur est connecté sur la page d'accueil
    When L'utilisateur renseigne les elements obligatoires    ${JDD1.facility}
    Sleep    5
    And l'utilisateur choisi Healthcare Program     ${JDD1.HealthcareProgram}
    And L'utilisateur click sur appointment
    Then Then le rendez-vous est confirmé    ${JDD1.VerifHealthcareProgram}

Test 2
    Open browser     ${url}    ${browser}

    Given L'utilisateur est connecté sur la page d'accueil
    When L'utilisateur renseigne les elements obligatoires    ${JDD2.facility}
    Sleep    5
    And l'utilisateur choisi Healthcare Program     ${JDD2.HealthcareProgram}
    And L'utilisateur click sur appointment
    Then Then le rendez-vous est confirmé    ${JDD2.VerifHealthcareProgram}

Test 3
    Open browser     ${url}    ${browser}

    Given L'utilisateur est connecté sur la page d'accueil
    When L'utilisateur renseigne les elements obligatoires    ${JDD3.facility}
    Sleep    5
    And l'utilisateur choisi Healthcare Program     ${JDD3.HealthcareProgram}
    And L'utilisateur click sur appointment
    Then Then le rendez-vous est confirmé    ${JDD3.VerifHealthcareProgram}